package com.example.denutrition;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;

public class Habits extends AppCompatActivity {

    Patient patient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_habits);
        getSupportActionBar().setTitle("Habitudes");

        patient = Memory.patientsList.get(Memory.currentPatientPosition);
    }

    public void appetitRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.habitsAppetitARB:
                if (checked)
                    patient.setPatientAppetit(0);
                break;
            case R.id.habitsAppetitBRB:
                if (checked)
                    patient.setPatientAppetit(1);
                break;
            case R.id.habitsAppetitCRB:
                if (checked)
                    patient.setPatientAppetit(2);
                break;
        }
    }

    public void medicRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.habitsMedicYRB:
                if (checked)
                    patient.setPatientMedic(0);
                break;
            case R.id.habitsMedicNRB:
                if (checked)
                    patient.setPatientMedic(1);
                break;
        }
    }

    public void mealsRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.habitsMealsARB:
                if (checked)
                    patient.setPatientMeals(0);
                break;
            case R.id.habitsMealsBRB:
                if (checked)
                    patient.setPatientMeals(1);
                break;
            case R.id.habitsMealsCRB:
                if (checked)
                    patient.setPatientMeals(2);
                break;
        }
    }

    public void eatRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.habitsEatARB:
                if (checked)
                    patient.setPatientEat(0);
                break;
            case R.id.habitsEatBRB:
                if (checked)
                    patient.setPatientEat(1);
                break;
            case R.id.habitsEatCRB:
                if (checked)
                    patient.setPatientEat(2);
                break;
        }
    }

    public void wellNourrishedRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.habitsWellNourrishedARB:
                if (checked)
                    patient.setPatientWellNourrished(0);
                break;
            case R.id.habitsWellNourrishedBRB:
                if (checked)
                    patient.setPatientWellNourrished(1);
                break;
            case R.id.habitsWellNourrishedCRB:
                if (checked)
                    patient.setPatientWellNourrished(2);
                break;
        }
    }

    public void healthRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.habitsHealthARB:
                if (checked)
                    patient.setPatientHealth(0);
                break;
            case R.id.habitsHealthBRB:
                if (checked)
                    patient.setPatientHealth(1/2);
                break;
            case R.id.habitsHealthCRB:
                if (checked)
                    patient.setPatientHealth(1);
                break;
            case R.id.habitsHealthDRB:
                if (checked)
                    patient.setPatientHealth(2);
                break;
        }
    }

    public void Save(View view) {
        Memory.patientsList.remove(Memory.currentPatientPosition);
        Memory.patientsList.add(Memory.currentPatientPosition, patient);
        this.finish();
    }
}
