package com.example.denutrition;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;

public class Login extends AppCompatActivity {

    EditText loginET;
    EditText passwordET;
    EditText ipAdressET;
    EditText portET;

    String login;
    String password;
    String ipAdress;
    String port;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginET = (EditText) findViewById(R.id.loginLoginET);
        passwordET = (EditText) findViewById(R.id.loginPasswordET);
        ipAdressET = (EditText) findViewById(R.id.loginIpAdressET);
        portET = (EditText) findViewById(R.id.loginPortET);
    }

    public void Login(View view) {
        ipAdress = ipAdressET.getText().toString();
        port = portET.getText().toString();

        login = loginET.getText().toString();
        password = passwordET.getText().toString();

        String[] strings = new String[4];
        strings[0] = ipAdress;
        strings[1] = port;
        strings[2] = login;
        strings[3] = password;

        new PostAsyncTask().execute(strings);

        /*if (login.equals("admin") && password.equals("bpm")) {
            Memory.userLogin = "admin";
            Memory.IPAdress = ipAdressET.getText().toString();
            startActivity(new Intent(getApplicationContext(), PatientsList.class));
        } else {
            DialogFragments.WrongIDDialogFragment wid = new DialogFragments.WrongIDDialogFragment();
            wid.show(getSupportFragmentManager(), "");
        }*/
    }

    private class PostAsyncTask extends AsyncTask<String, String, String> {

        URL url;
        String parameters;
        HttpURLConnection urlConnection;
        String userCredential;

        @Override
        protected String doInBackground(String... strings) {
            try {
                url = new URL("http://" + strings[0] + ":" + strings[1] + "/bonita-server-rest/API/managementAPI/checkUserCredentials/" + strings[2]);
                System.out.println(url);

                parameters = "password=" + strings[3] + "&options=user:" + strings[2];
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setUseCaches(false);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setInstanceFollowRedirects(false);
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setRequestProperty("Authorization", "Basic " + Base64Coder.encodeString("restuser:restbpm"));

                DataOutputStream out = new DataOutputStream(urlConnection.getOutputStream());
                out.writeBytes(parameters);
                out.flush();
                out.close();
                int responseCode = urlConnection.getResponseCode();
                System.out.println(responseCode);
                String response = urlConnection.getResponseMessage();
                System.out.println(response);

                if(responseCode == 200) {
                    InputStream is = urlConnection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                    // Parse response into String
                    String line;
                    StringBuffer responseBuffer = new StringBuffer();
                    try {
                        while((line = reader.readLine()) != null) {
                            responseBuffer.append(line);
                            responseBuffer.append('\n');
                        }
                    } finally {
                        reader.close();
                        is.close();
                    }
                    userCredential = responseBuffer.toString();
                    System.out.println("SERVER RESPONSE : " + responseBuffer.toString().trim());
                }

                urlConnection.disconnect();

                System.out.println("MESSAGE : " + urlConnection.getResponseMessage());

                } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ConnectException e) {
                DialogFragments.NoConnectionDialogFragment nc = new DialogFragments.NoConnectionDialogFragment();
                nc.show(getSupportFragmentManager(), "");
            } catch (SocketException e) {
                DialogFragments.SettingsProblemDialogFragment sp = new DialogFragments.SettingsProblemDialogFragment();
                sp.show(getSupportFragmentManager(), "");
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(userCredential);
            return userCredential;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (userCredential.contains("true")) {
                Memory.userLogin = "admin";
                Memory.IPAdress = ipAdressET.getText().toString();
                startActivity(new Intent(getApplicationContext(), PatientsList.class));
            } else if (userCredential.contains("false")) {
                DialogFragments.WrongIDDialogFragment wid = new DialogFragments.WrongIDDialogFragment();
                wid.show(getSupportFragmentManager(), "");
            } else {
                System.out.println("SHOULD NOT HAPPEN");
            }

        }
    }

}
