package com.example.denutrition;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class Diagnostic extends AppCompatActivity {

    Patient patient;

    EditText weightET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagnostic);
        getSupportActionBar().setTitle("Diagnostic");

        patient = Memory.patientsList.get(Memory.currentPatientPosition);

        weightET = (EditText) findViewById(R.id.diagnosticWeightET);
    }

    public void escarresRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.diagnosticEscarresYRB:
                if (checked)
                    patient.setPatientEscarres(0);
                break;
            case R.id.diagnosticEscarresNRB:
                if (checked)
                    patient.setPatientEscarres(1);
                break;
        }
    }

    public void bracCircRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.diagnosticBracCircARB:
                if (checked)
                    patient.setPatientBracCirc(0);
                break;
            case R.id.diagnosticBracCircBRB:
                if (checked)
                    patient.setPatientBracCirc(1);
                break;
            case R.id.diagnosticBracCircCRB:
                if (checked)
                    patient.setPatientBracCirc(2);
                break;
        }
    }

    public void molCircRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.diagnosticMolCircARB:
                if (checked)
                    patient.setPatientMolCirc(0);
                break;
            case R.id.diagnosticMolCircBRB:
                if (checked)
                    patient.setPatientMolCirc(1);
                break;
        }
    }

    public void Save(View view) {
        patient.setPatientWeight(Integer.valueOf(weightET.getText().toString()));

        Memory.patientsList.remove(Memory.currentPatientPosition);
        Memory.patientsList.add(Memory.currentPatientPosition, patient);
        this.finish();
    }
}
