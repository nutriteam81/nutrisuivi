package com.example.denutrition;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.Document;
import org.jdom2.xpath.XPath;

public class PatientsList extends AppCompatActivity {

    ListView patientsListLV;
    MyArrayAdapter adapter;

    Document document;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Liste des patients");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        //.setAction("Action", null).show();
                startActivity(new Intent(getApplicationContext(), AddPatient.class));
            }
        });

        new PostAsyncTask2().execute();

        Memory.patientsList = new ArrayList<>();

        patientsListLV = (ListView) findViewById(R.id.patientsListLV);
        adapter = new MyArrayAdapter(this, R.layout.custom_patients_list, Memory.patientsList);
        patientsListLV.setAdapter(adapter);

        patientsListLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Memory.currentPatientPosition = i;
                startActivity(new Intent(getApplicationContext(), MainScreen.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        adapter.notifyDataSetChanged();
    }

    private class PostAsyncTask2 extends AsyncTask<String, String, String> {

        URL url;
        String parameters;
        HttpURLConnection urlConnection;

        @Override
        protected String doInBackground(String... strings) {
            try {
                url = new URL("http://" + Memory.IPAdress + ":8080/bonita-server-rest/API/queryRuntimeAPI/getProcessInstances");
                System.out.println(url);

                parameters = "options=user:" + Memory.userLogin;
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setUseCaches(false);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setInstanceFollowRedirects(false);
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setRequestProperty("Authorization", "Basic " + Base64Coder.encodeString("restuser:restbpm"));

                DataOutputStream out = new DataOutputStream(urlConnection.getOutputStream());
                out.writeBytes(parameters);
                out.flush();
                out.close();
                int responseCode = urlConnection.getResponseCode();
                System.out.println(responseCode);
                String response = urlConnection.getResponseMessage();
                System.out.println(response);

                if(responseCode == 200) {
                    InputStream is = urlConnection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                    // Parse response into String
                    String line;
                    StringBuffer responseBuffer = new StringBuffer();
                    try {
                        while((line = reader.readLine()) != null) {
                            responseBuffer.append(line);
                            responseBuffer.append('\n');
                        }
                    } finally {
                        reader.close();
                        is.close();
                    }

                    SAXBuilder sxb = new SAXBuilder();
                    try {
                        document = sxb.build(new ByteArrayInputStream(responseBuffer.toString().trim().getBytes()));
                    } catch (JDOMException e) {
                        e.printStackTrace();
                    }

                    System.out.println(responseBuffer.toString().trim());
                }

                urlConnection.disconnect();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                XPath xpa = XPath.newInstance("//ProcessInstance");
                List<Element> processes = (List<Element>) xpa.selectNodes(document);

                for(Element el : processes) {
                    xpa = XPath.newInstance("./processUUID/value/text()");
                    String processDefinitionUUID = xpa.valueOf(el);

                    if (processDefinitionUUID.equals("Plan_de_soins--1.0")) {
                        xpa = XPath.newInstance("./variableUpdates/org.ow2.bonita.facade.runtime.impl.VariableUpdateImpl[name/text() = 'nomPatient']/clientVariable/text()");
                        String lastName = xpa.valueOf(el);
                        System.out.println("LASTNAME = " + lastName);

                        xpa = XPath.newInstance("./variableUpdates/org.ow2.bonita.facade.runtime.impl.VariableUpdateImpl[name/text() = 'prenomPatient']/clientVariable/text()");
                        String firstName = xpa.valueOf(el);
                        System.out.println("FIRSTNAME = " + firstName);

                        xpa = XPath.newInstance("./variableUpdates/org.ow2.bonita.facade.runtime.impl.VariableUpdateImpl[name/text() = 'taillePatient']/clientVariable/text()");
                        String height = xpa.valueOf(el);
                        System.out.println("HEIGHT = " + height);

                        xpa = XPath.newInstance("./variableUpdates/org.ow2.bonita.facade.runtime.impl.VariableUpdateImpl[name/text() = 'independancePatient']/clientVariable/text()");
                        String independency = xpa.valueOf(el);
                        System.out.println("INDEPENDENCY = " + independency);

                        xpa = XPath.newInstance("./variableUpdates/org.ow2.bonita.facade.runtime.impl.VariableUpdateImpl[name/text() = 'motricitePatient']/clientVariable/text()");
                        String motricity = xpa.valueOf(el);
                        System.out.println("MOTRICITY = " + motricity);

                        xpa = XPath.newInstance("./variableUpdates/org.ow2.bonita.facade.runtime.impl.VariableUpdateImpl[name/text() = 'maladieStressPatient']/clientVariable/text()");
                        String diseaseStress = xpa.valueOf(el);
                        System.out.println("DISEASESTRESS = " + diseaseStress);

                        xpa = XPath.newInstance("./variableUpdates/org.ow2.bonita.facade.runtime.impl.VariableUpdateImpl[name/text() = 'neuropsychoPatient']/clientVariable/text()");
                        String neuroPsycho = xpa.valueOf(el);
                        System.out.println("NEUROPSYCHO = " + neuroPsycho);

                        Patient newPatient = new Patient();
                        newPatient.setPatientLastName(lastName);
                        newPatient.setPatientFirstName(firstName);
                        newPatient.setPatientHeight(Integer.valueOf(height));
                        newPatient.setPatientIndependency(Integer.valueOf(independency));
                        newPatient.setPatientMotricity(Integer.valueOf(motricity));
                        newPatient.setPatientDiseaseStress(Integer.valueOf(diseaseStress));
                        newPatient.setPatientNeuroPsycho(Integer.valueOf(neuroPsycho));

                        Memory.patientsList.add(newPatient);
                    }

                }

                adapter.notifyDataSetChanged();

            } catch (JDOMException e) {
                e.printStackTrace();
            }
        }
    }

    private class MyArrayAdapter extends ArrayAdapter<Patient> {

        List<Patient> list;

        private MyArrayAdapter(Context context, int resource, List<Patient> objects) {
            super(context, resource, objects);
            this.list = objects;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewGroup layout = (ViewGroup) getLayoutInflater().inflate(R.layout.custom_patients_list, null);
            TextView patientNameTV = (TextView) layout.findViewById(R.id.customPatientsListTV);
            patientNameTV.setText(list.get(position).getPatientLastName() + " " + list.get(position).getPatientFirstName());
            return layout;
        }

    }

}
