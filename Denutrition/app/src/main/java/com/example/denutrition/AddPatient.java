package com.example.denutrition;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;

import java.util.Calendar;

public class AddPatient extends AppCompatActivity {

    final Calendar c = Calendar.getInstance();
    static int year;
    static int month;
    static int day;

    static Button birthDateBtn;

    Patient newPatient;

    EditText lastNameET;
    EditText firstNameET;
    EditText phoneET;
    EditText heightET;
    EditText addressET;
    EditText address2ET;
    EditText cityET;
    EditText postalCodeET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_patient);

        lastNameET = (EditText) findViewById(R.id.addPatientLastNameET);
        firstNameET = (EditText) findViewById(R.id.addPatientFirstNameET);
        phoneET = (EditText) findViewById(R.id.addPatientPhoneET);
        heightET = (EditText) findViewById(R.id.addPatientHeightET);
        addressET = (EditText) findViewById(R.id.addPatientAddressET);
        address2ET = (EditText) findViewById(R.id.addPatientAddress2ET);
        cityET = (EditText) findViewById(R.id.addPatientCityET);
        postalCodeET = (EditText) findViewById(R.id.addPatientPostalCodeET);

        birthDateBtn = (Button) findViewById(R.id.addPatientBirthDateBtn);

        //Set today date for birthDate picker
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        //birthDateBtn.setText(String.valueOf(day) + "/" + String.valueOf(month + 1) + "/" + String.valueOf(year));
        birthDateBtn.setText("09/02/1947");

        newPatient = new Patient();
    }

    public void sexRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.addPatientSexFRB:
                if (checked)
                    newPatient.setPatientSex("F");
                    break;
            case R.id.addPatientSexMRB:
                if (checked)
                    newPatient.setPatientSex("M");
                    break;
        }
    }

    public void birthDatePickerClicked(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void AddPatient(View view) {
        newPatient.setPatientLastName(lastNameET.getText().toString());
        newPatient.setPatientFirstName(firstNameET.getText().toString());
        //Vérifier que le sex est bien coché
        newPatient.setPatientSex("M");
        newPatient.setPatientBirthDate(String.valueOf(day) + "/" + String.valueOf(month) + "/" + String.valueOf(year));
        newPatient.setPatientPhone(phoneET.getText().toString());
        //Vérifier que la taille est bien un entier
        newPatient.setPatientHeight(Integer.valueOf(heightET.getText().toString()));
        newPatient.setPatientAddress(addressET.getText().toString());
        newPatient.setPatientAddress2(address2ET.getText().toString());
        newPatient.setPatientCity(cityET.getText().toString());
        newPatient.setPatientPostalCode(postalCodeET.getText().toString());

        Memory.patientsList.add(newPatient);
        Memory.currentPatientPosition = Memory.patientsList.size() - 1;

        startActivity(new Intent(getApplicationContext(), GlobalEvaluation.class));
        this.finish();
    }


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int yearP, int monthP, int dayP) {
            day = dayP;
            month = monthP;
            year = yearP;
            birthDateBtn.setText(String.valueOf(day) + "/" + String.valueOf(month + 1) + "/" + String.valueOf(year));
        }
    }
}
