package com.example.denutrition;

/**
 * Created by Skip on 19/01/2017.
 */

public class Patient {

    private String patientLastName;
    private String patientFirstName;
    private String patientSex;
    private String patientBirthDate;
    private String patientPhone;
    private int patientHeight;
    private String patientAddress;
    private String patientAddress2;
    private String patientCity;
    private String patientPostalCode;

    private int patientIndependency;
    private int patientMotricity;
    private int patientDiseaseStress;
    private int patientNeuroPsycho;

    private int patientMilkProducts;
    private int patientEggs;
    private int patientMeat;
    private int patientFruitsVegs;
    private int patientDrinks;

    private int patientAppetit;
    private int patientMedic;
    private int patientMeals;
    private int patientEat;
    private int patientWellNourrished;
    private float patientHealth;

    private int patientEscarres;
    private int patientBracCirc;
    private int patientMolCirc;
    private int patientWeight;

    private float patientScoreMNA;

    public Patient() {

    }

    public Patient(String patientLastName, String patientFirstName) {
        this.patientLastName = patientLastName;
        this.patientFirstName = patientFirstName;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }

    public String getPatientFirstName() {
        return patientFirstName;
    }

    public void setPatientFirstName(String patientFirstName) {
        this.patientFirstName = patientFirstName;
    }

    public String getPatientSex() {
        return patientSex;
    }

    public void setPatientSex(String patientSex) {
        this.patientSex = patientSex;
    }

    public String getPatientBirthDate() {
        return patientBirthDate;
    }

    public void setPatientBirthDate(String patientBirthDate) {
        this.patientBirthDate = patientBirthDate;
    }

    public String getPatientPhone() {
        return patientPhone;
    }

    public void setPatientPhone(String patientPhone) {
        this.patientPhone = patientPhone;
    }

    public int getPatientHeight() {
        return patientHeight;
    }

    public void setPatientHeight(int patientHeight) {
        this.patientHeight = patientHeight;
    }

    public String getPatientAddress() {
        return patientAddress;
    }

    public void setPatientAddress(String patientAddress) {
        this.patientAddress = patientAddress;
    }

    public String getPatientAddress2() {
        return patientAddress2;
    }

    public void setPatientAddress2(String patientAddress2) {
        this.patientAddress2 = patientAddress2;
    }

    public String getPatientCity() {
        return patientCity;
    }

    public void setPatientCity(String patientCity) {
        this.patientCity = patientCity;
    }

    public String getPatientPostalCode() {
        return patientPostalCode;
    }

    public void setPatientPostalCode(String patientPostalCode) {
        this.patientPostalCode = patientPostalCode;
    }

    public int getPatientIndependency() {
        return patientIndependency;
    }

    public void setPatientIndependency(int patientIndependency) {
        this.patientIndependency = patientIndependency;
    }

    public int getPatientMotricity() {
        return patientMotricity;
    }

    public void setPatientMotricity(int patientMotricity) {
        this.patientMotricity = patientMotricity;
    }

    public int getPatientDiseaseStress() {
        return patientDiseaseStress;
    }

    public void setPatientDiseaseStress(int patientDiseaseStress) {
        this.patientDiseaseStress = patientDiseaseStress;
    }

    public int getPatientNeuroPsycho() {
        return patientNeuroPsycho;
    }

    public void setPatientNeuroPsycho(int patientNeuroPsycho) {
        this.patientNeuroPsycho = patientNeuroPsycho;
    }

    public int getPatientMilkProducts() {
        return patientMilkProducts;
    }

    public void setPatientMilkProducts(int patientMilkProducts) {
        this.patientMilkProducts = patientMilkProducts;
    }

    public int getPatientEggs() {
        return patientEggs;
    }

    public void setPatientEggs(int patientEggs) {
        this.patientEggs = patientEggs;
    }

    public int getPatientMeat() {
        return patientMeat;
    }

    public void setPatientMeat(int patientMeat) {
        this.patientMeat = patientMeat;
    }

    public int getPatientFruitsVegs() {
        return patientFruitsVegs;
    }

    public void setPatientFruitsVegs(int patientFruitsVegs) {
        this.patientFruitsVegs = patientFruitsVegs;
    }

    public int getPatientDrinks() {
        return patientDrinks;
    }

    public void setPatientDrinks(int patientDrinks) {
        this.patientDrinks = patientDrinks;
    }

    public int getPatientAppetit() {
        return patientAppetit;
    }

    public void setPatientAppetit(int patientAppetit) {
        this.patientAppetit = patientAppetit;
    }

    public int getPatientMedic() {
        return patientMedic;
    }

    public void setPatientMedic(int patientMedic) {
        this.patientMedic = patientMedic;
    }

    public int getPatientMeals() {
        return patientMeals;
    }

    public void setPatientMeals(int patientMeals) {
        this.patientMeals = patientMeals;
    }

    public int getPatientEat() {
        return patientEat;
    }

    public void setPatientEat(int patientEat) {
        this.patientEat = patientEat;
    }

    public int getPatientWellNourrished() {
        return patientWellNourrished;
    }

    public void setPatientWellNourrished(int patientWellNourrished) {
        this.patientWellNourrished = patientWellNourrished;
    }

    public float getPatientHealth() {
        return patientHealth;
    }

    public void setPatientHealth(float patientHealth) {
        this.patientHealth = patientHealth;
    }

    public int getPatientEscarres() {
        return patientEscarres;
    }

    public void setPatientEscarres(int patientEscarres) {
        this.patientEscarres = patientEscarres;
    }

    public int getPatientBracCirc() {
        return patientBracCirc;
    }

    public void setPatientBracCirc(int patientBracCirc) {
        this.patientBracCirc = patientBracCirc;
    }

    public int getPatientMolCirc() {
        return patientMolCirc;
    }

    public void setPatientMolCirc(int patientMolCirc) {
        this.patientMolCirc = patientMolCirc;
    }

    public int getPatientWeight() {
        return patientWeight;
    }

    public void setPatientWeight(int patientWeight) {
        this.patientWeight = patientWeight;
    }

    public float getPatientScoreMNA() {
        return patientScoreMNA;
    }

    public void setPatientScoreMNA(float patientScoreMNA) {
        this.patientScoreMNA = patientScoreMNA;
    }
}
