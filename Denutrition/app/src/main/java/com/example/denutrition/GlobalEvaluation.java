package com.example.denutrition;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;

public class GlobalEvaluation extends AppCompatActivity {

    Patient newPatient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_evaluation);

        newPatient = Memory.patientsList.get(Memory.currentPatientPosition);
    }

    public void independancyRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.globalEvalIndependancyYRB:
                if (checked)
                    newPatient.setPatientIndependency(1);
                    break;
            case R.id.globalEvalIndependancyNRB:
                if (checked)
                    newPatient.setPatientIndependency(0);
                    break;
        }
    }

    public void motricityRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.globalEvalMotricityARB:
                if (checked)
                    newPatient.setPatientMotricity(0);
                    break;
            case R.id.globalEvalMotricityBRB:
                if (checked)
                    newPatient.setPatientMotricity(1);
                    break;
            case R.id.globalEvalMotricityCRB:
                if (checked)
                    newPatient.setPatientMotricity(2);
                    break;
        }
    }

    public void diseaseStressRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.globalEvalDiseaseStressYRB:
                if (checked)
                    newPatient.setPatientDiseaseStress(0);
                    break;
            case R.id.globalEvalDiseaseStressNRB:
                if (checked)
                    newPatient.setPatientDiseaseStress(2);
                    break;
        }
    }

    public void neuroRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.globalEvalNeuroARB:
                if (checked)
                    newPatient.setPatientNeuroPsycho(0);
                    break;
            case R.id.globalEvalNeuroBRB:
                if (checked)
                    newPatient.setPatientNeuroPsycho(1);
                    break;
            case R.id.globalEvalNeuroCRB:
                if (checked)
                    newPatient.setPatientNeuroPsycho(2);
                    break;
        }
    }

    public void Save(View view) {
        //Vérifier que toutes les infos sont entrées

        Memory.patientsList.remove(Memory.currentPatientPosition);
        Memory.patientsList.add(Memory.currentPatientPosition, newPatient);

        startActivity(new Intent(getApplicationContext(), MainScreen.class));

        this.finish();
    }
}
