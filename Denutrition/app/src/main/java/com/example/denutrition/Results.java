package com.example.denutrition;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Results extends AppCompatActivity {

    Patient patient;

    TextView scoreTV;
    TextView stateTV;

    float score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        getSupportActionBar().setTitle("Résultats");

        patient = Memory.patientsList.get(Memory.currentPatientPosition);

        scoreTV = (TextView) findViewById(R.id.resultsScoreTV);
        stateTV = (TextView) findViewById(R.id.resultsStateTV);

        scoreTV.setText(String.valueOf(patient.getPatientScoreMNA()));
        if (score < 17) {
            stateTV.setText("Mauvais état nutritionnel");
        } else if (score < 23.5) {
            stateTV.setText("Risque de malnutrition");
        } else {
            stateTV.setText("État nutritionnel normal");
        }
    }
}
