package com.example.denutrition;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class EditPatient extends AppCompatActivity {

    Patient patient;

    EditText lastNameET;
    EditText firstNameET;
    EditText phoneET;
    EditText heightET;
    EditText addressET;
    EditText address2ET;
    EditText cityET;
    EditText postalCodeET;

    RadioButton independancyYRB;
    RadioButton independancyNRB;
    RadioButton motricityARB;
    RadioButton motricityBRB;
    RadioButton motricityCRB;
    RadioButton diseaseStressYRB;
    RadioButton diseaseStressNRB;
    RadioButton neuroARB;
    RadioButton neuroBRB;
    RadioButton neuroCRB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_patient);

        patient = Memory.patientsList.get(Memory.currentPatientPosition);

        lastNameET = (EditText) findViewById(R.id.editPatientLastNameET);
        firstNameET = (EditText) findViewById(R.id.editPatientFirstNameET);
        phoneET = (EditText) findViewById(R.id.editPatientPhoneET);
        heightET = (EditText) findViewById(R.id.editPatientHeightET);
        addressET = (EditText) findViewById(R.id.editPatientAddressET);
        address2ET = (EditText) findViewById(R.id.editPatientAddress2ET);
        cityET = (EditText) findViewById(R.id.editPatientCityET);
        postalCodeET = (EditText) findViewById(R.id.editPatientPostalCodeET);

        independancyYRB = (RadioButton) findViewById(R.id.editPatientIndependancyYRB);
        independancyNRB = (RadioButton) findViewById(R.id.editPatientIndependancyNRB);
        motricityARB = (RadioButton) findViewById(R.id.editPatientMotricityARB);
        motricityBRB = (RadioButton) findViewById(R.id.editPatientMotricityBRB);
        motricityCRB = (RadioButton) findViewById(R.id.editPatientMotricityCRB);
        diseaseStressYRB = (RadioButton) findViewById(R.id.editPatientDiseaseStressYRB);
        diseaseStressNRB = (RadioButton) findViewById(R.id.editPatientDiseaseStressNRB);
        neuroARB = (RadioButton) findViewById(R.id.editPatientNeuroARB);
        neuroBRB = (RadioButton) findViewById(R.id.editPatientNeuroBRB);
        neuroCRB = (RadioButton) findViewById(R.id.editPatientNeuroCRB);

        lastNameET.setText(patient.getPatientLastName());
        firstNameET.setText(patient.getPatientFirstName());
        phoneET.setText(patient.getPatientPhone());
        heightET.setText(String.valueOf(patient.getPatientHeight()));
        addressET.setText(patient.getPatientAddress());
        address2ET.setText(patient.getPatientAddress2());
        cityET.setText(patient.getPatientCity());
        postalCodeET.setText(patient.getPatientPostalCode());

        if (patient.getPatientIndependency() == 1) {
            independancyYRB.setChecked(true);
            independancyNRB.setChecked(false);
        } else if (patient.getPatientIndependency() == 0) {
            independancyYRB.setChecked(false);
            independancyNRB.setChecked(true);
        } else {
            independancyYRB.setChecked(false);
            independancyNRB.setChecked(false);
        }

        if (patient.getPatientMotricity() == 0) {
            motricityARB.setChecked(true);
            motricityBRB.setChecked(false);
            motricityCRB.setChecked(false);
        } else if (patient.getPatientMotricity() == 1) {
            motricityARB.setChecked(false);
            motricityBRB.setChecked(true);
            motricityCRB.setChecked(false);
        } else if (patient.getPatientMotricity() == 2) {
            motricityARB.setChecked(false);
            motricityBRB.setChecked(false);
            motricityCRB.setChecked(true);
        } else {
            motricityARB.setChecked(false);
            motricityBRB.setChecked(false);
            motricityCRB.setChecked(false);
        }

        if (patient.getPatientDiseaseStress() == 0) {
            diseaseStressYRB.setChecked(true);
            diseaseStressNRB.setChecked(false);
        } else if (patient.getPatientDiseaseStress() == 2) {
            diseaseStressYRB.setChecked(false);
            diseaseStressNRB.setChecked(true);
        } else {
            diseaseStressYRB.setChecked(false);
            diseaseStressNRB.setChecked(false);
        }

        if (patient.getPatientNeuroPsycho() == 0) {
            neuroARB.setChecked(true);
            neuroBRB.setChecked(false);
            neuroCRB.setChecked(false);
        } else if (patient.getPatientNeuroPsycho() == 1) {
            neuroARB.setChecked(false);
            neuroBRB.setChecked(true);
            neuroCRB.setChecked(false);
        } else if (patient.getPatientNeuroPsycho() == 2) {
            neuroARB.setChecked(false);
            neuroBRB.setChecked(false);
            neuroCRB.setChecked(true);
        } else {
            neuroARB.setChecked(false);
            neuroBRB.setChecked(false);
            neuroCRB.setChecked(false);
        }
    }

    public void independancyRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.globalEvalIndependancyYRB:
                if (checked)
                    patient.setPatientIndependency(1);
                break;
            case R.id.globalEvalIndependancyNRB:
                if (checked)
                    patient.setPatientIndependency(0);
                break;
        }
    }

    public void motricityRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.globalEvalMotricityARB:
                if (checked)
                    patient.setPatientMotricity(0);
                break;
            case R.id.globalEvalMotricityBRB:
                if (checked)
                    patient.setPatientMotricity(1);
                break;
            case R.id.globalEvalMotricityCRB:
                if (checked)
                    patient.setPatientMotricity(2);
                break;
        }
    }

    public void diseaseStressRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.globalEvalDiseaseStressYRB:
                if (checked)
                    patient.setPatientDiseaseStress(0);
                break;
            case R.id.globalEvalDiseaseStressNRB:
                if (checked)
                    patient.setPatientDiseaseStress(2);
                break;
        }
    }

    public void neuroRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.globalEvalNeuroARB:
                if (checked)
                    patient.setPatientNeuroPsycho(0);
                break;
            case R.id.globalEvalNeuroBRB:
                if (checked)
                    patient.setPatientNeuroPsycho(1);
                break;
            case R.id.globalEvalNeuroCRB:
                if (checked)
                    patient.setPatientNeuroPsycho(2);
                break;
        }
    }

    public void Save(View view) {
        patient.setPatientLastName(lastNameET.getText().toString());
        patient.setPatientFirstName(firstNameET.getText().toString());
        patient.setPatientPhone(phoneET.getText().toString());
        //Vérifier que la taille est bien un entier
        patient.setPatientHeight(Integer.valueOf(heightET.getText().toString()));
        patient.setPatientAddress(addressET.getText().toString());
        patient.setPatientAddress2(address2ET.getText().toString());
        patient.setPatientCity(cityET.getText().toString());
        patient.setPatientPostalCode(postalCodeET.getText().toString());

        Memory.patientsList.remove(Memory.currentPatientPosition);
        Memory.patientsList.add(Memory.currentPatientPosition, patient);
        this.finish();
    }
}
