package com.example.denutrition;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MNA extends AppCompatActivity {

    Patient patient;

    TextView scoreTV;
    TextView stateTV;

    float score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mna);
        getSupportActionBar().setTitle("MNA");

        patient = Memory.patientsList.get(Memory.currentPatientPosition);

        scoreTV = (TextView) findViewById(R.id.mnaScoreTV);
        stateTV = (TextView) findViewById(R.id.mnaStateTV);
    }

    public void Calculate(View view) {
        score = 0;

        score += patient.getPatientIndependency();
        score += patient.getPatientMotricity();
        score += patient.getPatientDiseaseStress();
        score += patient.getPatientNeuroPsycho();

        float k = patient.getPatientMilkProducts() + patient.getPatientEggs() + patient.getPatientMeat();
        if (k <= 1) {
            score += 0;
        } else if (k == 2) {
            score += 0.5;
        } else {
            score += 1;
        }
        score += patient.getPatientFruitsVegs();
        score += patient.getPatientDrinks() / 2;

        score += patient.getPatientAppetit();
        score += patient.getPatientMedic();
        score += patient.getPatientMeals();
        score += patient.getPatientEat();
        score += patient.getPatientWellNourrished();
        score += patient.getPatientHealth();

        int IMC = patient.getPatientWeight() / (patient.getPatientHeight() * patient.getPatientHeight() / 10000);
        if (IMC < 19) {
            score += 0;
        } else if (IMC < 21) {
            score += 1;
        } else if (IMC < 23) {
            score += 2;
        } else {
            score += 3;
        }
        score += patient.getPatientEscarres();
        score += patient.getPatientBracCirc() / 2;
        score += patient.getPatientMolCirc();

        scoreTV.setText(String.valueOf(score));
        Memory.patientsList.get(Memory.currentPatientPosition).setPatientScoreMNA(score);
        if (score < 17) {
            stateTV.setText("Mauvais état nutritionnel");
        } else if (score < 23.5) {
            stateTV.setText("Risque de malnutrition");
        } else {
            stateTV.setText("État nutritionnel normal");
        }
    }
}
