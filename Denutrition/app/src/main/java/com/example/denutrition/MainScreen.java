package com.example.denutrition;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainScreen extends AppCompatActivity {

    TextView patientNameTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        getSupportActionBar().setTitle("Patient");

        patientNameTV = (TextView) findViewById(R.id.mainScreenPatientNameTV);
        patientNameTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        patientNameTV.setText(Memory.patientsList.get(Memory.currentPatientPosition).getPatientLastName() + " " + Memory.patientsList.get(Memory.currentPatientPosition).getPatientFirstName());

        patientNameTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), EditPatient.class));
            }
        });
    }

    public void openNutrition(View view) {

        startActivity(new Intent(getApplicationContext(), Nutrition.class));

    }

    public void openHabits(View view) {

        startActivity(new Intent(getApplicationContext(), Habits.class));

    }

    public void openDiagnostic(View view) {

        startActivity(new Intent(getApplicationContext(), Diagnostic.class));

    }

    public void openMNA(View view) {

        startActivity(new Intent(getApplicationContext(), MNA.class));

    }

    public void openResults(View view) {

        startActivity(new Intent(getApplicationContext(), Results.class));

    }
}
