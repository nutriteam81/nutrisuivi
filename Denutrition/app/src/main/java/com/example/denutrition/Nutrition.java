package com.example.denutrition;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;

public class Nutrition extends AppCompatActivity {

    Patient patient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nutrition);
        getSupportActionBar().setTitle("Nutrition");

        patient = Memory.patientsList.get(Memory.currentPatientPosition);
    }

    public void milkProductsRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.nutritionMilkProductsYRB:
                if (checked)
                    patient.setPatientMilkProducts(1);
                break;
            case R.id.nutritionMilkProductsNRB:
                if (checked)
                    patient.setPatientMilkProducts(0);
                break;
        }
    }

    public void eggsRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.nutritionEggsYRB:
                if (checked)
                    patient.setPatientEggs(1);
                break;
            case R.id.nutritionEggsNRB:
                if (checked)
                    patient.setPatientEggs(0);
                break;
        }
    }

    public void meatRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.nutritionMeatYRB:
                if (checked)
                    patient.setPatientMeat(1);
                break;
            case R.id.nutritionMeatNRB:
                if (checked)
                    patient.setPatientMeat(0);
                break;
        }
    }

    public void fruitsVegsRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.nutritionFruitsVegsYRB:
                if (checked)
                    patient.setPatientFruitsVegs(1);
                break;
            case R.id.nutritionFruitsVegsNRB:
                if (checked)
                    patient.setPatientFruitsVegs(0);
                break;
        }
    }

    public void drinksRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.nutritionDrinksARB:
                if (checked)
                    patient.setPatientDrinks(0);
                break;
            case R.id.nutritionDrinksBRB:
                if (checked)
                    patient.setPatientDrinks(1);
                break;
            case R.id.nutritionDrinksCRB:
                if (checked)
                    patient.setPatientDrinks(2);
                break;
        }
    }

    public void Save(View view) {
        Memory.patientsList.remove(Memory.currentPatientPosition);
        Memory.patientsList.add(Memory.currentPatientPosition, patient);
        this.finish();
    }

    /*private class MyPagerAdapter extends PagerAdapter {
        ArrayList<String> liste;

        public MyPagerAdapter(ArrayList<String> liste, Context context){
            this.liste = liste;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ViewGroup layout = (ViewGroup) View.inflate(getApplicationContext(), R.layout.custom_nutrition_pager, null);
            container.addView(layout);

            TextView questionTV = (TextView) layout.findViewById(R.id.nutritionPagerQuestionTV);
            TextView numberTV = (TextView) layout.findViewById(R.id.nutritionPagerNumberTV);

            String question = liste.get(position);

            int num = position + 1;

            questionTV.setText(question);
            numberTV.setText(num + "/" + liste.size());

            return layout;
        }

        @Override
        public int getCount() {
            return liste.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }*/
}
